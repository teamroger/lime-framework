<?php

/**
 * Generic configuration class for storing arbitrary data under string keys.
 */
class Lime_Config
{
    protected $values = array();
    
    public function set($key, $value)
    {
        if (is_string($key)) {
            $this->values[$key] = $value;
            return $this;
        }
        
        throw new Exception('Key must be a string.');
    }
    
    public function has($key)
    {
        if (is_string($key)) {
            return isset($this->values[$key]);
        }
        
        throw new Exception('Key must be a string.');
    }
    
    public function get($key)
    {
        if ($this->has($key)) {
            return $this->values[$key];
        }
        
        throw new Exception('Key "' . $key . '" does not exist.');
    }
    
    public function remove($key)
    {
        if ($this->has($key)) {
            unset($this->values[$key]);
            return true;
        }
        
        return false;
    }
}
