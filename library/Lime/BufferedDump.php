<?php

class Lime_BufferedDump
{
    const VAR_DUMP = 0;
    const PRINT_R = 1;
    protected $varToDump = null;
    protected $dumpType = 0;
    
    public function __construct($varToDump, $dumpType = 0)
    {
        $this->varToDump = $varToDump;
        $this->dumpType = $dumpType;
    }
    
    public function __toString()
    {
        ob_start();
        switch ($this->dumpType) {
            case 0:
                var_dump($this->varToDump);
                break;
                
            case 1:
                print_r($this->varToDump);
                break;
                
            default:
                echo (string)$this->varToDump;
                break;
        }
            
        $contents = ob_get_contents();
        ob_end_clean();
        
        return $contents;
    }
}
