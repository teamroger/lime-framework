<?php

class Lime_IoC_Instantiator
{
    public function instantiate($className, array $args)
    {
        $class = new ReflectionClass($className);
        $object = $class->newInstanceArgs($args);
        
        return $object;
    }
}
