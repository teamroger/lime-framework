<?php

interface Lime_IoC_IFactory
{
    public function resolve();
}
