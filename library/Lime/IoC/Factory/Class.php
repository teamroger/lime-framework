<?php

class Lime_IoC_Factory_Class implements Lime_IoC_IFactory
{
    protected $container = null;
    protected $className = '';
    protected $dependencyNames = array();
    protected $dependencyObjects = array();
    
    public function __construct(
        Lime_IoC_Container $container,
        $className,
        array $dependencyNames
    ) {
        $this->container = $container;
        $this->className = (string) $className;
        $this->dependencyNames = $dependencyNames;
    }
    
    public function getContainer()
    {
        return $this->container;
    }
    
    public function getClassName()
    {
        return $this->className;
    }
    
    public function getDependencyNames()
    {
        return $this->dependencyNames;
    }
    
    public function resolve()
    {
        if (! empty($this->dependencyNames)
            && empty($this->dependencyObjects)
        ) {
            // The dependency objects should mimic the constructor parameter list.
            // Even if they have the same type of object needed more than once.
            // E.G.- Car(wheel, wheel, wheel, wheel)
            // should fill the dependency-object array with 4 wheels, not 1.
            foreach ($this->dependencyNames as $dn) {
                $this->dependencyObjects[] = $this->container->resolve($dn);
            }
        }
        
        return $this
            ->container
            ->getInstantiator()
            ->instantiate(
                $this->className,
                $this->dependencyObjects
            );
    }
}
