<?php

interface Lime_IoC_Factory_CustomInterface
{
    public function resolve(Lime_IoC_Container $container);
}
