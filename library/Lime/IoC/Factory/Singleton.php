<?php

class Lime_IoC_Factory_Singleton extends Lime_IoC_Factory_Class
{
    protected $instance = null;
    
    public function resolve()
    {
        if ($this->instance === null) {
            $this->instance = parent::resolve();
        }
        
        return $this->instance;
    }
}
