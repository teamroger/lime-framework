<?php

class Lime_IoC_Container
{
    protected $baseValues = array();
    protected $factorys = array();
    protected $instantiator = null;
    
    public function getInstantiator()
    {
        if ($this->instantiator === null) {
            $this->instantiator = new Lime_IoC_Instantiator();
        }
        
        return $this->instantiator;
    }
    
    public function registerBaseValues(array $baseValues)
    {
        $this->baseValues = $baseValues;
        
        return $this;
    }
    
    public function register(
        $name,
        $className,
        $dependencies = array(),
        $singleton = false
    ) {
        if ($singleton) {
            $this->factorys[$name] = new Lime_IoC_Factory_Singleton(
                $this,
                $className,
                $dependencies
            );
        } else {
            $this->factorys[$name] = new Lime_IoC_Factory_Class(
                $this,
                $className,
                $dependencies
            );
        }
        
        return $this;
    }
    
    public function registerCustom(
        $dependencyName,
        Lime_IoC_Factory_CustomInterface $customFactory
    ) {
        $this->factorys[$dependencyName] = $customFactory;
        
        return $this;
    }
    
    public function resolve($name)
    {
        if (isset($this->baseValues[$name])) {
            return $this->baseValues[$name];
        }
        
        if (isset($this->factorys[$name])) {
            if ($this->factorys[$name] instanceof Lime_IoC_Factory_CustomInterface) {
                return $this->factorys[$name]->resolve($this);
            }
            
            return $this->factorys[$name]->resolve();
        }
        
        throw new Lime_Exception('"' . $name . '" was not registered.');
    }
}
