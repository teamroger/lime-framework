<?php

interface Lime_Http_Request_SourceInterface
{
    public function getMethod();
    public function getUri();
    public function getQueryString();
    public function getHttpVersion();
    public function getHeaders();
    public function getBody();
}
