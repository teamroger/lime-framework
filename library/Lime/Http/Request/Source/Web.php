<?php

class Lime_Http_Request_Source_Web implements Lime_Http_Request_SourceInterface
{
    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
    
    public function getUri()
    {
        $fullUri = $_SERVER['REQUEST_URI'];
        
        $uriParts = explode('?', $fullUri);
        
        return reset($uriParts);
    }
    
    public function getQueryString()
    {
        return $_SERVER['QUERY_STRING'];
    }
    
    public function getHttpVersion()
    {
        return (float) substr($_SERVER['SERVER_PROTOCOL'], -3);
    }
    
    public function getHeaders()
    {
        $headers = array();
        
        foreach ($_SERVER as $k => $v) {
            // Take all the "HTTP_" keys in $_SERVER.
            if (substr($k, 0, 5) === 'HTTP_') {
                // Remove the "HTTP_" part.
                $k = substr($k, 5);
                
                // Change underscores to spaces.
                $k = str_replace('_', ' ', $k);
                
                // Lowercase everything and then uppercase word beginnings.
                $k = ucwords(strtolower($k));
                
                // Change the space characters to hyphens.
                $k = str_replace(' ', '-', $k);
                
                // Set the key and value pairs.
                $headers[$k] = $v;
            }
        }
        
        return $headers;
    }
    
    public function getBody()
    {
        return file_get_contents('php://input');
    }
}
