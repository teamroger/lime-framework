<?php

class Lime_Http_Request_Method
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const HEAD = 'HEAD';
    const TRACE = 'TRACE';
    const OPTIONS = 'OPTIONS';
    const CONNECT = 'CONNECT';
    
    protected $methods = array();
    protected $method = '';
    
    public function __construct($method)
    {
        $this->methods = array(
        self::GET, self::POST, self::PUT, self::DELETE,
        self::HEAD, self::TRACE, self::OPTIONS, self::CONNECT
        );
        
        $this->setValue($method);
    }
    
    public function setValue($method)
    {
        $ucMethod = strtoupper((string) $method);
        
        if (! in_array($ucMethod, $this->methods)) {
            throw new Lime_Http_Exception_BadRequest(
                'Invalid HTTP request method: "' . $method . '"'
            );
        }
        
        $this->method = $ucMethod;
    }
    
    public function getValue()
    {
        return $this->method;
    }
    
    public function isGet()
    {
        return $this->method === self::GET;
    }
    
    public function isPost()
    {
        return $this->method === self::POST;
    }
    
    public function isPut()
    {
        return $this->method === self::PUT;
    }
    
    public function isDelete()
    {
        return $this->method === self::DELETE;
    }
    
    public function isHead()
    {
        return $this->method === self::HEAD;
    }
    
    public function isTrace()
    {
        return $this->method === self::TRACE;
    }
    
    public function isOptions()
    {
        return $this->method === self::OPTIONS;
    }
    
    public function isConnect()
    {
        return $this->method === self::CONNECT;
    }
    
    public function __toString()
    {
        return $this->method;
    }
}
