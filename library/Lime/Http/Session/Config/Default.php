<?php

class Lime_Http_Session_Config_Default implements Lime_Http_Session_ConfigInterface
{
    public function getKey()
    {
        return 'LIME_SESSION_ID';
    }
    
    public function getLifetime()
    {
        return 60 * 10;
    }
    
    public function getStorage()
    {
        $dir = realpath('.');
        $storage = new Lime_Http_Session_Storage_File($dir);
        
        return $storage;
    }
}
