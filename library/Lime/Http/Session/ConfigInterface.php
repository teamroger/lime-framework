<?php

interface Lime_Http_Session_ConfigInterface
{
    public function getKey();
    public function getLifetime();
    public function getStorage();
}
