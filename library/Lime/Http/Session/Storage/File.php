<?php

class Lime_Http_Session_Storage_File implements Lime_Http_Session_StorageInterface
{
    protected $dir = '';
    
    public function __construct($dir)
    {
        $this->setDir($dir);
    }
    
    public function setDir($dir)
    {
        if (! is_dir($dir)) {
            throw new Lime_Http_Exception_InternalError(
                'No such directory: "' . $dir . '"'
            );
        }
        
        if (! is_readable($dir)) {
            throw new Lime_Http_Exception_InternalError(
                'Directory is not readable: "' . $dir . '"'
            );
        }
        
        if (! is_writable($dir)) {
            throw new Lime_Http_Exception_InternalError(
                'Directory is not writable: "' . $dir . '"'
            );
        }
        
        $this->dir = $dir;
    }
    
    public function save($id, array $data)
    {
        $filename = $this->dir .DIRECTORY_SEPARATOR . $id . '.session';
        
        file_put_contents($filename, serialize($data));
    }
    
    public function has($id)
    {
        $filename = $this->dir .DIRECTORY_SEPARATOR . $id . '.session';
        
        return file_exists($filename);
    }
    
    public function get($id)
    {
        $filename = $this->dir .DIRECTORY_SEPARATOR . $id . '.session';
        
        return unserialize(file_get_contents($filename));
    }
    
    public function destroy($id)
    {
        $filename = $this->dir .DIRECTORY_SEPARATOR . $id . '.session';
        
        if (file_exists($filename)) {
            unlink($filename);
        }
    }
    
    public function touch($id)
    {
        $filename = $this->dir .DIRECTORY_SEPARATOR . $id . '.session';
        
        touch($filename);
    }
    
    public function isExpired($id, $expiry)
    {
        if ($expiry < 0) {
            return false;
        }
        
        $filename = $this->dir .DIRECTORY_SEPARATOR . $id . '.session';
        
        return $this->fileIsExpired($filename, $expiry);
    }
    
    public function cleanUp($expiry)
    {
        $di = new DirectoryIterator($this->dir);
        
        foreach ($di as $file) {
            $isDot = $file->isDot();
            $isDir = $file->isDir();
            $isSession = substr($file->getFilename(), -8) === '.session';
            
            if (!$isDot && !$isDir && $isSession) {
                if ($this->fileIsExpired($file->getPathname(), $expiry)) {
                    unlink($file->getPathname());
                }
            }
        }
    }
    
    protected function fileIsExpired($filename, $expiry)
    {
        $modified = filemtime($filename);
        $validUntil = $modified + $expiry;
        
        if (time() > $validUntil) {
            return true;
        }
        
        return false;
    }
}
