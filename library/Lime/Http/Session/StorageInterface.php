<?php

interface Lime_Http_Session_StorageInterface
{
    public function save($id, array $data);
    public function has($id);
    public function get($id);
    public function destroy($id);
    public function touch($id);
    public function isExpired($id, $expiry);
    public function cleanUp($expiry);
}
