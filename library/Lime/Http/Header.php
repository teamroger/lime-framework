<?php

class Lime_Http_Header extends Lime_Http_Header_Abstract
{
    /*
    protected $name;
    protected $value;
    */
    
    public function __construct($name, $value)
    {
        $this->setName($name);
        $this->setValue($value);
    }
    
    public function setName($name)
    {
        $this->name = (string) $name;
        
        return $this;
    }
    
    /*
    public function getName() {
    return $this->name;
    }

    public function setValue($value) {
    $this->value = (string) $value;

    return $this;
    }

    public function getValue() {
    return $this->value;
    }

    public function __toString() {
    return $this->getName() . ': ' . $this->getValue();
    }
    */
}
