<?php

// Possibly a useless class.
// Just create a header object,
// create an outgoing cookie object,
// and put the cookie in the header
// with the name "Set-Cookie".
class Lime_Http_Header_Cookie_Outgoing extends Lime_Http_Header_Abstract
{
    public function __construct(Lime_Http_Cookie_Outgoing $cookie)
    {
        $this->name = 'Set-Cookie';
        $this->setValue($value);
    }
    
    public function setValue(Lime_Http_Cookie_Outgoing $cookie)
    {
        parent::setValue($cookie);
    }
}
