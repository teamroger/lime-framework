<?php

abstract class Lime_Http_Header_Abstract
{
    protected $name = '';
    protected $value = '';
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function setValue($value)
    {
        $this->value = (string) $value;
        
        return $this;
    }
    
    public function __toString()
    {
        return $this->getName() . ': ' . $this->getValue();
    }
}
