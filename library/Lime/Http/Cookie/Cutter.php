<?php

class Lime_Http_Cookie_Cutter
{
    public function cut($cookieHeader)
    {
        $cookies = array();
        
        $cookiePairs = explode(';', $cookieHeader);
        
        foreach ($cookiePairs as $pair) {
            list($name, $value) = explode('=', $pair);
            
            $name = urldecode(trim($name));
            $value = urldecode(trim($value));
            
            $cookies[] = new Lime_Http_Cookie_Incoming($name, $value);
        }
        
        return $cookies;
    }
}
