<?php

class Lime_Http_Cookie_Jar
{
    protected $response = null;
    protected $cutter = null;
    protected $incoming = array();
    protected $outgoing = array();
    
    public function __construct(
        Lime_Http_Cookie_Cutter $cutter,
        Lime_Http_Response $response
    ) {
        $this->cutter = $cutter;
        $this->response = $response;
    }
    
    public function init(Lime_Http_Request $request)
    {
        $cutter = $this->cutter;
        $headers = $request->getHeaders();
        
        foreach ($headers as $header) {
            if ($header->getName() === 'Cookie') {
                $cookies = $cutter->cut($header->getValue());
                
                foreach ($cookies as $cookie) {
                    $this->add($cookie);
                }
            }
        }
    }
    
    public function add(Lime_Http_Cookie_Incoming $cookie)
    {
        $this->incoming[] = $cookie;
        
        return $this;
    }
    
    public function has($name)
    {
        foreach ($this->incoming as $cookie) {
            if ($cookie->getName() === $name) {
                return true;
            }
        }
        
        return false;
    }
    
    public function get($name)
    {
        foreach ($this->incoming as $cookie) {
            if ($cookie->getName() === $name) {
                return $cookie;
            }
        }
        
        return null;
    }
    
    public function set(
        Lime_Http_Cookie_Outgoing $cookie
    ) {
        $this->outgoing[] = $cookie;
        
        $this->response->addHeader(
            new Lime_Http_Header('Set-Cookie', $cookie)
        );
        
        return $this;
    }
    
    public function getIncoming()
    {
        return $this->incoming;
    }
    
    public function getOutgoing()
    {
        return $this->outgoing;
    }
}
