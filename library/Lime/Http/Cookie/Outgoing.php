<?php

class Lime_Http_Cookie_Outgoing extends Lime_Http_Cookie_Abstract
{
    protected $expires;
    protected $maxAge;
    protected $domain;
    protected $path;
    protected $secure;
    protected $httpOnly;
    protected $sent;
    
    public function __construct(
        $name,
        $value,
        $expires = null,
        $maxAge = null,
        $domain = '',
        $path = '',
        $secure = false,
        $httpOnly = false,
        $sent = false
    ) {
        parent::__construct($name, $value);
        $this
            ->setExpires($expires)
            ->setMaxAge($maxAge)
            ->setDomain($domain)
            ->setPath($path)
            ->setSecure($secure)
            ->setHttpOnly($httpOnly)
            ->setSent($sent);
    }
    
    public function setExpires($unixTime)
    {
        $this->expires = $unixTime;
        
        return $this;
    }
    
    public function getExpires()
    {
        return $this->expires;
    }
    
    public function setMaxAge($seconds)
    {
        $this->maxAge = $seconds;
        
        return $this;
    }
    
    public function getMaxAge()
    {
        return $this->maxAge;
    }
    
    public function setDomain($domain)
    {
        $this->domain = (string) $domain;
        
        return $this;
    }
    
    public function getDomain()
    {
        return $this->domain;
    }
    
    public function setPath($path)
    {
        $this->path = (string) $path;
        
        return $this;
    }
    
    public function getPath()
    {
        return $this->path;
    }
    
    public function setSecure($secure)
    {
        $this->secure = $secure;
        
        return $this;
    }
    
    public function getSecure()
    {
        return $this->secure;
    }
    
    public function setHttpOnly($httpOnly)
    {
        $this->httpOnly = $httpOnly;
        
        return $this;
    }
    
    public function getHttpOnly()
    {
        return $this->httpOnly;
    }
    
    public function setSent($sent)
    {
        $this->sent = (boolean) $sent;
    }
    
    public function getSent()
    {
        return $this->sent;
    }
    
    public function __toString()
    {
        $strings = array();
        
        $strings[] = $this->getName() . '=' . $this->getValue();
        
        if ($this->getExpires() !== null) {
            $strings[] = 'Expires=' .
            date('l, d-M-Y H:i:s', $this->getExpires()) .
            ' GMT';
        }
        
        if ($this->getMaxAge() !== null) {
            $strings[] = 'Max-Age=' . $this->getMaxAge();
        }
        
        if ($this->getDomain() !== '') {
            $strings[] = 'Domain=' . $this->getDomain();
        }
        
        if ($this->getPath() !== '') {
            $strings[] = 'Path=' . $this->getPath();
        }
        
        if ($this->getSecure()) {
            $strings[] = 'Secure';
        }
        
        if ($this->getHttpOnly()) {
            $strings[] = 'HttpOnly';
        }
        
        $cookieString = implode('; ', $strings);
        
        return $cookieString;
    }
}
