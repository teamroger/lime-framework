<?php

class Lime_Http_Response
{
    protected $httpVersion;
    protected $code;
    protected $phrase;
    protected $headers;
    protected $body;
    
    public function __construct()
    {
        $this->httpVersion = 0;
        $this->code = 0;
        $this->phrase = '';
        $this->headers = array();
        $this->body = '';
    }
    
    public function setHttpVersion($version)
    {
        $version = (float) $version;
        
        if ($version !== 1 && $version !== 1.1) {
            throw new Lime_Http_Exception_InternalError(
                'Invalid HTTP version: "' . $version . '"'
            );
        }
        
        $this->httpVersion = $version;
        
        return $this;
    }
    
    public function getHttpVersion()
    {
        return $this->httpVersion;
    }
    
    public function setCode($code)
    {
        $code = (int) $code;
        
        if ($code >= 100 && $code < 600) {
            $this->code = $code;
            
            return $this;
        } else {
            throw new Lime_Http_Exception_InternalError(
                'Invalid response code set: "' . $code . '"'
            );
        }
    }
    
    public function getCode()
    {
        return $this->code;
    }
    
    public function setPhrase($phrase)
    {
        $this->phrase = (string) $phrase;
        
        return $this;
    }
    
    public function getPhrase()
    {
        return $this->phrase;
    }
    
    public function getResponseLine()
    {
        $responseLine = 'HTTP/' .
        number_format($this->getHttpVersion(), 1) .
        ' ' .
        $this->getCode() .
        ' ' .
        $this->getPhrase();
        
        return $responseLine;
    }
    
    public function addHeader(Lime_Http_Header $header, $replace = true)
    {
        if ($replace) {
            // Remove all previous headers with the same name.
            foreach ($this->headers as $key => $value) {
                if ($value->getName() === $header->getName()) {
                    unset($this->headers[$key]);
                }
            }
        }
        
        $this->headers[] = $header;
        
        return $this;
    }
    
    public function getHeaders()
    {
        return $this->headers;
    }
    
    public function removeHeader(Lime_Http_Header $header)
    {
        $somethingWasRemoved = false;
        
        foreach ($this->headers as $key => $value) {
            if ($value === $header) {
                unset($this->headers[$key]);
                $somethingWasRemoved = true;
            }
        }
        
        return $somethingWasRemoved;
    }
    
    public function setBody($body)
    {
        $this->body = (string) $body;
        
        return $this;
    }
    
    public function getBody()
    {
        return $this->body;
    }
    
    public function append($bodyData)
    {
        $this->body .= $bodyData;
        
        return $this;
    }
    
    public function __toString()
    {
        $string = $this->getResponseLine() . "\r\n";
        
        foreach ($this->headers as $header) {
            $string .= (string) $header . "\r\n";
        }
        
        $string .= "\r\n" . $this->getBody() . "\r\n";
        
        return $string;
    }
}
