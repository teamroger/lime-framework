<?php

class Lime_Http_Response_Sender
{
    protected $response = null;
    protected $plugins = array();
    
    public function setPlugin($key, Lime_Http_Response_Sender_PluginInterface $plugin)
    {
        $this->plugins[$key] = $plugin;
        
        return $this;
    }
    
    public function removePlugin($key)
    {
        if (array_key_exists($key, $this->plugins)) {
            unset($this->plugins[$key]);
            
            return true;
        }
        
        return false;
    }
    
    public function send(Lime_Http_Response $response)
    {
        $this->response = $response;
        
        $this->preSend();
        
        $this->sendResponseLine();
        $this->sendHeaders();
        $this->sendBody();
        
        $this->postSend();
    }
    
    public function sendResponseLine()
    {
        header($this->response->getResponseLine());
    }
    
    public function sendHeaders()
    {
        $headers = $this->response->getHeaders();
        
        foreach ($headers as $header) {
            header((string) $header);
        }
    }
    
    public function sendBody()
    {
        echo $this->response->getBody();
    }
    
    protected function preSend()
    {
        foreach ($this->plugins as $plugin) {
            $plugin->preSend($this->response);
        }
    }
    
    protected function postSend()
    {
        foreach ($this->plugins as $plugin) {
            $plugin->postSend($this->response);
        }
    }
}
