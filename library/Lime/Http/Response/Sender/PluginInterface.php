<?php

interface Lime_Http_Response_Sender_PluginInterface
{
    public function preSend(Lime_Http_Response $response);
    public function postSend(Lime_Http_Response $response);
}
