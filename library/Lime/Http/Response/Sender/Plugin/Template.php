<?php

class Lime_Http_Response_Sender_Plugin_Template implements
    Lime_Http_Response_Sender_PluginInterface
{
    protected $template;
    
    public function __construct(Lime_Template $template)
    {
        $this->template = $template;
    }
    
    public function preSend(Lime_Http_Response $response)
    {
        $header = new Lime_Http_Response_Header('Content-Type', 'text/html');
        
        $response
            ->addHeader($header)
            ->setBody($this->template);
    }
    
    public function postSend(Lime_Http_Response $response)
    {
        // Do nothing.
    }
}
