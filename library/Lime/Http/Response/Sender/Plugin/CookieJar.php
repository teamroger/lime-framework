<?php

class Lime_Http_Response_Sender_Plugin_CookieJar implements
    Lime_Http_Response_Sender_PluginInterface
{
    protected $jar;
    
    public function __construct(Lime_Http_Cookie_Jar $jar)
    {
        $this->setCookieJar($jar);
    }
    
    public function setCookieJar(Lime_Http_Cookie_Jar $jar)
    {
        $this->jar = $jar;
    }
    
    public function preSend(Lime_Http_Response $response)
    {
        $cookies = $this->jar->getOutgoing();
        
        foreach ($cookies as $cookie) {
            $header = new Lime_Http_Header(
                'Set-Cookie',
                (string) $cookie
            );
            
            $response->addHeader($header, false);
        }
    }
    
    public function postSend(Lime_Http_Response $response)
    {
        // Do nothing.
    }
}
