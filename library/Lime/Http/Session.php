<?php

class Lime_Http_Session
{
    protected $key = null;
    protected $jar = null;
    protected $storage = null;
    protected $lifetime = 0;
    protected $generator = null;
    protected $id = null;
    protected $session = array();
    protected $setCookie = true;
    
    public function __construct(
        $key,
        Lime_Http_Cookie_Jar $jar,
        Lime_Http_Session_StorageInterface $storage,
        $lifetime,
        Lime_String_RandomGenerator $generator
    ) {
        $this->key = $key;
        $this->jar = $jar;
        $this->storage = $storage;
        $this->lifetime = $lifetime;
        $this->generator = $generator;
    }
    
    public function init()
    {
        $this->cleanUp();
        
        $jar = $this->getJar();
        $key = $this->getKey();
        $storage = $this->getStorage();
        $generateId = true;
        
        if ($jar->has($key)) {
            $cookie = $jar->get($key);
            $id = $cookie->getValue();
            
            if ($this->idIsSafe($id) && $storage->has($id)) {
                if ($storage->isExpired($id, $this->lifetime)) {
                    $storage->destroy($id);
                    $this->unsetCookie($id);
                } else {
                    $this->session = $storage->get($id);
                    $storage->touch($id);
                    $this->setCookie = false;
                    $this->id = $id;
                    $generateId = false;
                }
            }
        }
        
        if ($generateId) {
            $this->generateId();
        }
    }
    
    public function getKey()
    {
        return $this->key;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getJar()
    {
        return $this->jar;
    }
    
    public function getStorage()
    {
        return $this->storage;
    }
    
    public function has($key)
    {
        return isset($this->session[$key]);
    }
    
    public function get($key)
    {
        return $this->session[$key];
    }
    
    public function set($key, $value)
    {
        $this->session[$key] = $value;
        
        $storage = $this->getStorage();
        $storage->save($this->getId(), $this->session);
        
        if ($this->setCookie) {
            $this->getJar()->set(
                $this->createCookie(
                    $this->getKey(),
                    $this->getId()
                )
            );
            
            $this->setCookie = false;
        }
    }
    
    public function remove($key)
    {
        unset($this->session[$key]);
        
        $storage = $this->getStorage();
        $storage->save($this->getId(), $this->session);
    }
    
    public function cleanUp()
    {
        // To prevent the time expensive operation
        // of searching all of the storage and deleting
        // expired data, only do it roughly
        // one in ten times that a session is used.
        if (rand(0, 100) > 90) {
            $this->storage->cleanUp($this->lifetime);
        }
    }
    
    public function toArray()
    {
        return $this->session;
    }
    
    protected function generateId()
    {
        do {
            $this->id = $this->generator->generate(20);
        } while ($this->getStorage()->has($this->id));
    }
    
    protected function idIsSafe($id)
    {
        // IDs taken from a cookie sent to the server
        // could get used as part of a filename.
        // In this case the cookie could be used in a malicious way
        // e.g.- containing ../../file-gets-overwritten
        if (preg_match('/^[a-zA-Z0-9]+$/', $id)) {
            return true;
        } else {
            return false;
        }
    }
    
    protected function createCookie($name, $value)
    {
        $cookie = new Lime_Http_Cookie_Outgoing(
            $name,
            $value
        );
        
        $cookie->setPath('/');
        
        return $cookie;
    }
    
    protected function unsetCookie($id)
    {
        $cookie = $this->createCookie($this->getKey, $id);
        $cookie->setExpires(1);
        
        $this->jar->set($cookie);
    }
}
