<?php

class Lime_Http_Server
{
    protected $request = null;
    protected $response = null;
    protected $cookieJar = null;
    protected $session = null;
    protected $responseSender = null;
    protected $sendResponse = true;
    
    public function __construct(
        Lime_Http_Request $request,
        Lime_Http_Response $response,
        Lime_Http_Cookie_Jar $jar,
        Lime_Http_Session $session,
        Lime_Http_Response_Sender $sender
    ) {
        $this->request = $request;
        $this->response = $response;
        $this->cookieJar = $jar;
        $this->session = $session;
        $this->responseSender = $sender;
    }
    
    public function init()
    {
        $this->cookieJar->init($this->request);
        $this->session->init();
    }
    
    public function getRequest()
    {
        return $this->request;
    }
    
    public function getResponse()
    {
        return $this->response;
    }
    
    public function getCookieJar()
    {
        return $this->cookieJar;
    }
    
    public function getSession()
    {
        return $this->session;
    }
    
    public function responseEnabled()
    {
        return $this->sendResponse;
    }
    
    public function enableResponse()
    {
        $this->sendResponse = true;
    }
    
    public function preventResponse()
    {
        $this->sendResponse = false;
    }
    
    public function sendResponse()
    {
        if ($this->sendResponse) {
            $this->responseSender->send($this->response);
        }
    }
}
