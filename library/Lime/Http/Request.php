<?php

class Lime_Http_Request
{
    protected $method;
    protected $uri;
    protected $queryString;
    protected $httpVersion;
    protected $headers;
    protected $body;
    
    protected $queryVars = array();
    protected $bodyVars = array();
    
    public function __construct()
    {
        $this->method = null;
        $this->uri = '';
        $this->queryString = '';
        $this->httpVersion = '';
        $this->headers = array();
        $this->body = '';
    }
    
    public function setMethod(Lime_Http_Request_Method $method)
    {
        $this->method = $method;
        
        return $this;
    }
    
    public function getMethod()
    {
        return $this->method;
    }
    
    public function setUri($uri)
    {
        $this->uri = (string) $uri;
        
        return $this;
    }
    
    public function getUri()
    {
        return $this->uri;
    }
    
    public function getQueryString()
    {
        return $this->queryString;
    }
    
    public function setQueryString($queryString)
    {
        $this->queryString = (string) $queryString;
        parse_str($this->queryString, $this->queryVars);
        
        return $this;
    }
    
    public function setHttpVersion($version)
    {
        $version = (float) $version;
        
        if ($version !== 1.0 && $version !== 1.1) {
            throw new Lime_Http_Exception_BadRequest(
                'Invalid HTTP version: "' . (string) $version . '"'
            );
        }
        
        $this->httpVersion = $version;
        
        return $this;
    }
    
    public function getHttpVersion()
    {
        return $this->httpVersion;
    }
    
    public function addHeader(Lime_Http_Header $header)
    {
        $this->headers[] = $header;
        
        return $this;
    }
    
    public function getHeaders()
    {
        return $this->headers;
    }
    
    public function removeHeader(Lime_Http_Header $header)
    {
        $somethingWasRemoved = false;
        
        foreach ($this->headers as $key => $value) {
            if ($value === $header) {
                unset($this->headers[$key]);
                $somethingWasRemoved = true;
            }
        }
        
        return $somethingWasRemoved;
    }
    
    public function setBody($body)
    {
        $this->body = (string) $body;
        parse_str($this->body, $this->bodyVars);
        
        return $this;
    }
    
    public function getBody()
    {
        return $this->body;
    }
    
    public function queryVars($key = null)
    {
        if ($key === null) {
            return $this->queryVars;
        } elseif (isset($this->queryVars[$key])) {
            return $this->queryVars[$key];
        } else {
            return null;
        }
    }
    
    public function bodyVars($key = null)
    {
        if ($key === null) {
            return $this->bodyVars;
        } elseif (isset($this->bodyVars[$key])) {
            return $this->bodyVars[$key];
        } else {
            return null;
        }
    }
    
    public function __toString()
    {
        $queryString = $this->getQueryString();
        
        if (strlen($queryString) > 0) {
            $queryString = '?' . $queryString;
        }
        
        return
        $this->getMethod() .
        ' ' .
        $this->getUri() .
        $queryString .
        ' ' .
        'HTTP/' .
        number_format($this->getHttpVersion(), 1) .
        "\r\n" .
        $this->presentHeaders() .
        "\r\n\r\n" .
        $this->getBody();
    }
    
    protected function presentHeaders()
    {
        $temp = array();
        
        foreach ($this->headers as $header) {
            $temp[] = (string) $header;
        }
        
        $headers = implode("\r\n", $temp);
        
        return $headers;
    }
}
