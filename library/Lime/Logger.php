<?php

class Lime_Logger
{
    private $filename = '';
    
    public function setFilename($filename)
    {
        $this->filename = (string)$filename;
        
        return $this;
    }
    
    public function getFilename()
    {
        return $this->filename;
    }
    
    public function log($message)
    {
        $filename = $this->getFilename();
        $dir = dirname($filename);
        
        if (empty($filename)) {
            throw new Lime_Logger_Exception('Filename cannot be empty');
        }
        
        if (! is_dir($dir)) {
            throw new Lime_Logger_Exception('Directory does not exist: "' . $dir . '"');
        }
        
        if (! is_writable($dir)) {
            throw new Lime_Logger_Exception('Directory is nor writable: "' . $dir . '"');
        }
        
        $date = date('Y-m-d H:i:s');
        
        $message = (string)$message;
        $message = $date . "\n" . $message;
        $message .= "\n--------\n\n";
        
        file_put_contents($filename, $message, FILE_APPEND);
    }
}
