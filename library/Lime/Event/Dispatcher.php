<?php

/**
 * An abstract event dispatcher class to provide an event system.
 * A class that extends this can have event listener objects attached to it.
 * An event can then be triggered (dispatched) so that all listener objects for that type
 * of event can respond to it.
 */
abstract class Lime_Event_Dispatcher
{
    protected $listeners = array();
    
    /**
     * Adds an event listener.
     *
     * @param  string              $type     The type of event.
     * @param  Lime_Event_Listener $listener The event listener to respond to the event.
     * @return \Lime_Event_Dispatcher This object.
     */
    public function addEventListener($type, Lime_Event_Listener $listener)
    {
        if (!isset($this->listeners[$type])) {
            $this->listeners[$type] = array();
        }
        
        array_push($this->listeners[$type], $listener);
        
        return $this;
    }
    
    /**
     * Determines if this object has a particular listener bound to a particular event type.
     *
     * @param  string              $type     The type of event.
     * @param  Lime_Event_Listener $listener The event listener to look for.
     * @return boolean True if a listener was found for the event type, otherwise false.
     */
    public function hasEventListener($type, Lime_Event_Listener $listener)
    {
        if (isset($this->listeners[$type])) {
            foreach ($this->listeners[$type] as $lsnr) {
                if ($lsnr === $listener) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Removes an event listener bound to a paricular event type. The same listener may be bound
     * to the same event type more than onece, in which case all instances of it will be removed.
     * If the $listener parameter is left as null then all listeners for the given event type
     * will be removed.
     *
     * @param  string              $type     The type of event.
     * @param  Lime_Event_Listener $listener The event listener to remove.
     * @return boolean True if one or more listeners were removed, otherwise false.
     */
    public function removeEventListener($type, Lime_Event_Listener $listener = null)
    {
        $removed = false;
        $t = (string) $type;
        
        if ($listener === null) {
            if (isset($this->listeners[$t]) && count($this->listeners[$t]) > 0) {
                unset($this->listeners[$t]);
                
                return true;
            } else {
                return false;
            }
        }
        
        if (isset($this->listeners[$t])) {
            foreach ($this->listeners[$t] as $key => $value) {
                if ($value === $listener) {
                    unset($this->listeners[$t][$key]);
                    $removed = true;
                }
            }
        }
        
        return $removed;
    }
    
    /**
     * Dispatches an event to each listener bound to the event's type.
     *
     * @param  Lime_Event $event The event to dispatch.
     * @return \Lime_Event_Dispatcher This object.
     */
    public function dispatch(Lime_Event $event)
    {
        if (isset($this->listeners[$event->getType()])) {
            foreach ($this->listeners[$event->getType()] as $listener) {
                $listener->respondTo($event);
            }
        }
        
        return $this;
    }
}
