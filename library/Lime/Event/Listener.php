<?php

interface Lime_Event_Listener
{
    public function respondTo(Lime_Event $event);
}
