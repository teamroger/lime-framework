<?php

class Lime_Event
{
    private $type = 'event';
    private $target = null;
    private $timestamp = 0;
    
    public function __construct($type, $target = null)
    {
        $this->type = (string)$type;
        $this->setTarget($target);
        $this->timestamp = time();
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function getTarget()
    {
        return $this->target;
    }
    
    public function setTarget($target = null)
    {
        if (is_object($target) || $target === null) {
            $this->target = $target;
            return this;
        }
        
        throw new Exception('Event target must be an object or null.');
    }
    
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}
