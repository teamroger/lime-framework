<?php

class Lime_Template implements Lime_Template_Interface
{
    private $filename = '';
    private $data = array();

    public function __construct($filename = '', array $data = array())
    {
        $this->setFilename($filename);
        $this->data = $data;
    }

    public function getFilename()
    {
        return $this->filename;
    }
    
    public function setFilename($filename)
    {
        $this->filename = (string)$filename;
        
        return $this;
    }
    
    public function set($key, $value)
    {
        $key = (string)$key;

        if (!preg_match('/^[a-zA-Z_][0-9a-zA-Z_]*$/', $key)) {
            throw new Exception('Invalid key: "' . $key . '"');
        }

        $this->data[$key] = $value;

        return $this;
    }
    
    public function getOutput()
    {
        if (! file_exists($this->getFilename())) {
            throw new Exception(
                'File does not exist: "' .
                $this->getFilename() .
                '".'
            );
        }
        
        if (! is_readable($this->getFilename())) {
            throw new Exception(
                'File is not readable: "' .
                $this->getFilename() .
                '".'
            );
        }
        
        extract($this->data);
        
        ob_start();
        include $this->filename;
        $output = ob_get_contents();
        ob_end_clean();
        
        return $output;
    }

    public function __toString()
    {
        try {
            $output = $this->getOutput();
        } catch (Exception $e) {
            $output = (string) $e;
        }
        
        return $output;
    }
}
