<?php

class Lime_System implements Lime_SystemInterface
{
    protected $systems;
    
    public function __construct(array $systems)
    {
        $this->systems = $systems;
    }
    
    public function init(Lime_Registry $registry)
    {
        foreach ($this->systems as $class => $dependencies) {
            $this->loadSystem($registry, $class, $dependencies);
        }
    }
    
    public function getRegistryKeysCreated()
    {
        return array();
    }
    
    protected function loadSystem(
        Lime_Registry $registry,
        $class,
        array $dependencies
    ) {
        $dependencyObjects = array();
        
        foreach ($dependencies as $key) {
            if ($registry->has($key)) {
                $dependencyObjects[] = $registry->get($key);
            }
        }
        
        $reflectionClass = new ReflectionClass($class);
        $object = $reflectionClass->newInstanceArgs($dependencyObjects);
        
        $method = new ReflectionMethod($class, 'init');
        $method->invokeArgs($object, array($registry));
    }
}

// This is a special program, double A systrana, z...zero zredit.
