<?php

class Lime_Application_Factory_Route implements Lime_IoC_Factory_CustomInterface
{
    public function resolve(Lime_IoC_Container $container)
    {
        $request = $container->resolve('request');
        $table = $container->resolve('routeTable');
        
        $route = $table->match(
            $request->getMethod()->getValue(),
            $request->getUri()
        );
        
        return $route ? $route : null;
    }
}
