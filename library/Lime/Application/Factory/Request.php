<?php

class Lime_Application_Factory_Request implements Lime_IoC_Factory_CustomInterface
{
    public function resolve(Lime_IoC_Container $container)
    {
        $source = $container->resolve('requestSource');
        
        $request = new Lime_Http_Request();
        
        $request->setMethod(
            new Lime_Http_Request_Method($source->getMethod())
        );
        
        $request
            ->setUri($source->getUri())
            ->setQueryString($source->getQueryString())
            ->setHttpVersion($source->getHttpVersion());
        
        foreach ($source->getHeaders() as $key => $value) {
            $request->addHeader(
                new Lime_Http_Header($key, $value)
            );
        }
        
        $request->setBody($source->getBody());
        
        return $request;
    }
}
