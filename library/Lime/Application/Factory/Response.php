<?php

class Lime_Application_Factory_Response implements Lime_IoC_Factory_CustomInterface
{
    public function resolve(Lime_IoC_Container $container)
    {
        $response = new Lime_Http_Response();
        
        $response
            ->setHttpVersion(1.1)
            ->setCode(200)
            ->setPhrase('OK')
            ->setBody('')
            ->addHeader(new Lime_Http_Header('Content-Type', 'text/html'));
        
        return $response;
    }
}
