<?php

class Lime_Application_Factory_Controller implements Lime_IoC_Factory_CustomInterface
{
    protected $controllers = array();
    
    public function resolve(Lime_IoC_Container $container)
    {
        $route = $container->resolve('route');
        $namingConvention = $container->resolve('namingConvention');
        
        if ($route === null) {
            throw new Lime_Http_Exception_BadRequest('Route not found');
        }
        
        $name = $route->getResourceType();
        
        if (! isset($this->controllers[$name])) {
            $filepath = $namingConvention->formatFilename($name);
            
            if (! file_exists($filepath)) {
                throw new Lime_Http_Exception_InternalError(
                    'File not found: "' . $filepath . '"'
                );
            }
            
            include_once $filepath;
            
            $classname = $namingConvention->formatClassname($name);
            
            if (! class_exists($classname)) {
                throw new Lime_Http_Exception_InternalError(
                    'Class does not exist: "' . $classname . '"'
                );
            }
            
            $class = new ReflectionClass($classname);
            $instance = $class->newInstanceArgs(
                array(
                    $container->resolve('server'),
                    $route->getAction(),
                    $route->getParameters()
                )
            );
            
            if (! ($instance instanceof Lime_Controller_Action)) {
                   $message = 'Class "' . get_class($instance) . '" ' .
                     'must extend "Lime_Controller_Action".';
                   throw new Lime_Http_Exception_InternalError($message);
            }
            
            $this->controllers[$name] = $instance;
        }
        
        return $this->controllers[$name];
    }
}
