<?php

class Lime_Application_Container extends Lime_IoC_Container
{
    public function init()
    {
        $this
            ->register('application', 'Lime_Application', array('server', 'appSetup', 'actionController'), true)
            ->registerCustom('actionController', new Lime_Application_Factory_Controller())
            ->registerCustom('route', new Lime_Application_Factory_Route())
            ->register('server', 'Lime_Http_Server', array('request', 'response', 'cookieJar', 'session', 'responseSender'), true)
            ->registerCustom('request', new Lime_Application_Factory_Request())
            ->register('requestSource', 'Lime_Http_Request_Source_Web', array(), true)
            ->registerCustom('response', new Lime_Application_Factory_Response())
            ->register('responseSender', 'Lime_Http_Response_Sender', array(), true)
            ->register('cookieJar', 'Lime_Http_Cookie_Jar', array('cookieCutter', 'response'), true)
            ->register('cookieCutter', 'Lime_Http_Cookie_Cutter', array(), true)
            ->register(
                'session',
                'Lime_Http_Session',
                array(
                    'sessionKey',
                    'cookieJar',
                    'sessionStorage',
                    'sessionLifetime',
                    'stringGenerator'
                ),
                true
            )
            ->register('stringGenerator', 'Lime_String_RandomGenerator', array(), true);
    }
}
