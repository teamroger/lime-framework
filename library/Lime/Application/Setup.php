<?php

abstract class Lime_Application_Setup
{
    protected $actionController = null;
    
    public function run(Lime_Controller_Action $actionController)
    {
        $this->actionController = $actionController;
        
        $methods = get_class_methods($this);
        
        foreach ($methods as $method) {
            if ($this->isSetupMethod($method)) {
                $this->$method();
            }
        }
    }
    
    public function getActionController()
    {
        return $this->actionController;
    }
    
    /**
     * Determines if a method is callable and begins with 'setup'.
     */
    protected function isSetupmethod($methodName)
    {
        if (is_callable(array($this, $methodName))) {
            $prefix = substr($methodName, 0, 5);

            if ($prefix === 'setup') {
                return true;
            } else {
                return false;
            }
        }
    }
}
