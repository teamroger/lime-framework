<?php

class Lime_Post
{
    public static function has($key)
    {
        return Lime_Global_Post::getInstance()->has($key);
    }
    
    public static function get($key)
    {
        return Lime_Global_Post::getInstance()->get($key);
    }
}
