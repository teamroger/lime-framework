<?php

/**
 * A class for creating string objects and allowing any number
 * of string operations to be chained together onto it.
 */
class Lime_String
{
    protected static $operations = array();
    
    protected $string;
    
    /**
     * Creates and returns a new string object.
     */
    public static function create($string)
    {
        return new Lime_String($string);
    }
    
    /**
     * Constructs a new string object.
     * This method is not public.
     */
    protected function __construct($string)
    {
        $this->string = (string) $string;
    }
    
    /**
     * "Magic method" to be used with the intention of
     * applying string operations to this string.
     *
     * @param  $method string The method name that will be used as a basis for
     * creating a Lime_String_Operation to apply to this string.
     * @param  $args array The arguments that need to be passed into the operation
     * for it to do its job properly.
     * @return A newly transformed Lime_String object.
     */
    public function __call($method, array $args)
    {
        if (! isset(self::$operations[$method])) {
            $operation = self::_loadOperation($method);
            self::_addOperation($method, $operation);
        }
        
        $operation = self::$operations[$method];
        $operation->preApply($args);
        
        return $operation->apply($this);
    }
    
    /**
     * Allow the echo-ing of a String object to echo the encapsulated string value.
     */
    public function __toString()
    {
        return $this->string;
    }
    
    /**
     * Attempts to find and open a Lime_String_Operation file,
     * create an instance of the class within and return it.
     *
     * @param  $name string The simple name of the class,
     * for example "reverse" instead of Lime_String_Operation_Reverse.
     * @return The newly created instance of the operation.
     */
    protected static function loadOperation($name)
    {
        $path =
        realpath(dirname(__FILE__)) .
        DIRECTORY_SEPARATOR .
        'String' .
        DIRECTORY_SEPARATOR .
        'Operation' .
        DIRECTORY_SEPARATOR .
        ucfirst($name) .
        '.php';
        
        if (! file_exists($path) && is_readable($path)) {
            throw new Exception('Expecting readable file to exist: "' . $path . '"');
        }
        
        include_once $path;
        
        $classname = 'Lime_String_Operation_' . ucfirst($name);
        
        if (! class_exists($classname)) {
            throw new Exception('Expecting class to exist: "' . $classname . '"');
        }
        
        $object = new $classname();
        
        return $object;
    }
    
    /**
     * Adds an operation instance to the static cache.
     *
     * @param $name string The simple name of the operation.
     * @param $operation Lime_String_OperationInterface The operation to store.
     */
    protected static function addOperation(
        $name,
        Lime_String_OperationInterface $operation
    ) {
        self::$operations[$name] = $operation;
    }
}
