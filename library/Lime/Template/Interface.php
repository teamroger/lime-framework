<?php

interface Lime_Template_Interface
{
    public function getFilename();
    public function setFilename($filename);
    public function set($key, $data);
    public function getOutput();
    public function __toString();
}
