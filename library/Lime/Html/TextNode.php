<?php

class Lime_Html_TextNode implements Lime_Html_Node
{
    protected $text;
    
    public function __construct($text)
    {
        $this->setText($text);
    }
    
    public function setText($text)
    {
        $this->text = (string) $text;
        return $this;
    }
    
    public function getText()
    {
        return $this->text;
    }
    
    public function __toString()
    {
        return $this->getText();
    }
}
