<?php

class Lime_Html_Element implements Lime_Html_Node
{
    protected $tagName;
    protected $selfClosing;
    protected $attributes;
    protected $children;
    
    public function __construct($tagName, $selfClosing = false)
    {
        $this->setTagName($tagName);
        $this->selfClosing = $selfClosing;
        $this->attributes = array();
        $this->children = array();
    }
    
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;
        return $this;
    }
        
    public function getTagName()
    {
        return $this->tagName;
    }
    
    public function addAttribute($attribute, $value = null)
    {
        $this->attributes[$attribute] = $value;
        return $this;
    }
    
    public function addChild(Lime_Html_Node $child)
    {
        if ($this->selfClosing) {
            throw new Exception('Self closing tags cannot have child elements');
        }
        
        $this->children[] = $child;
        return $this;
    }
    
    public function __toString()
    {
        $html = $this->createTagStart();
        
        foreach ($this->children as $child) {
            $html .= (string) $child;
        }
        
        $html .= $this->createTagEnd();
        
        return $html;
    }
    
    protected function createTagStart()
    {
        $tag = '<' . $this->getTagName();
        
        foreach ($this->attributes as $key => $value) {
            $tag .= ' ' . $key;
            
            if ($value !== null) {
                $tag .= '="' . $value . '"';
            }
        }
        
        $tag .= $this->selfClosing ? ' />' : '>';
        
        return $tag;
    }
    
    protected function createTagEnd()
    {
        return $this->selfClosing ? '' : '</' . $this->getTagName() . '>';
    }
}
