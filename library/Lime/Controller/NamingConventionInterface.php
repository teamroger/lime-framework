<?php

interface Lime_Controller_NamingConventionInterface
{
    public function formatFilename($controllerName);
    public function formatClassname($controllerName);
}
