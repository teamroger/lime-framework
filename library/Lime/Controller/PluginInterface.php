<?php

interface Lime_Controller_PluginInterface
{
    public function preDispatch(Lime_Controller_Action $controller);
    public function postDispatch(Lime_Controller_Action $controller);
}
