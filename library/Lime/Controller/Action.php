<?php

abstract class Lime_Controller_Action
{
    private $server = null;
    protected $action = '';
    protected $parameters = array();
    private $plugins = array();
    
    public function __construct(Lime_Http_Server $server, $action, array $parameters)
    {
        $this->server = $server;
        $this->action = $action;
        $this->parameters = $parameters;
    }
    
    public function getServer()
    {
        return $this->server;
    }
    
    public function setPlugin($key, Lime_Controller_PluginInterface $plugin)
    {
        $this->plugins[$key] = $plugin;
        
        return $this;
    }
    
    public function getPlugin($key)
    {
        return $this->plugins[$key];
    }
    
    public function removePlugin($key)
    {
        if (array_key_exists($key, $this->plugins)) {
            unset($this->plugins[$key]);
            
            return true;
        }
        
        return false;
    }
    
    public function dispatch()
    {
        $methodExists = method_exists($this, $this->action);
        $methodIsCallable = is_callable(array($this, $this->action));
        
        if (! ($methodExists && $methodIsCallable)) {
            throw new Lime_Http_Exception_NotFound(
                'Action not found: "' . $this->action . '"'
            );
        }
        
        $this->preDispatchPlugins();
        
        $method = new ReflectionMethod($this, $this->action);
        $method->invokeArgs($this, $this->parameters);
        
        $this->postDispatchPlugins();
    }
    
    protected function preDispatchPlugins()
    {
        foreach ($this->plugins as $key => $plugin) {
            $plugin->preDispatch($this);
        }
    }
    
    protected function postDispatchPlugins()
    {
        foreach ($this->plugins as $key => $plugin) {
            $plugin->postDispatch($this);
        }
    }
}
