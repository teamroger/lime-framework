<?php

class Lime_Route_Table
{
    protected $table = array();
    
    public function set($requestMethod, array $routings)
    {
        $method = (string) $requestMethod;
        
        if (! isset($this->table[$requestMethod])) {
            $this->table[$method] = array();
        }
        
        foreach ($routings as $pattern => $route) {
            $this->table[$method][(string) $pattern]  = $route;
        }
    }
    
    public function match($requestMethod, $uri, $wildcardDelimiter = ':')
    {
        if (! array_key_exists($requestMethod, $this->table)) {
            return false;
        }
        
        $bindings = $this->table[$requestMethod];
        
        foreach ($bindings as $pattern => $route) {
            $patternObject = new Lime_Route_Pattern($pattern, $wildcardDelimiter);
            
            $parameters = $patternObject->match($uri);
            
            if ($parameters !== false) {
                foreach ($parameters as $name => $value) {
                    $route->addParameter($name, $value);
                };
                
                return $route;
            }
        }
        
        return false;
    }
}
