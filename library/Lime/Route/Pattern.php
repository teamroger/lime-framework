<?php

class Lime_Route_Pattern
{
    protected $pattern;
    protected $wildcardDelimiter;
    
    public function __construct($pattern = '', $wildcardDelimiter = ':')
    {
        $this->set($pattern);
        $this->setWildcardDelimiter($wildcardDelimiter);
    }
    
    public function get()
    {
        return $this->pattern;
    }
    
    public function set($pattern)
    {
        $this->pattern = (string) $pattern;
        
        return $this;
    }
    
    public function getWildcardDelimiter()
    {
        return $this->wildcardDelimiter;
    }
    
    public function setWildcardDelimiter($delimiter)
    {
        $this->wildcardDelimiter = (string) $delimiter;
        
        return $this;
    }
    
    public function match($uri)
    {
        // Separate the parts of the URIs.
        $patternParts = explode('/', $this->pattern);
        $uriParts = explode('/', $uri);
        
        // Compare the sizes.
        if (count($patternParts) !== count($uriParts)) {
            return false;
        }
        
        $parameters = array();
        $wd = $this->getWildcardDelimiter();
        
        foreach ($patternParts as $key => $string) {
            $uriPart = $uriParts[$key];
            
            if (substr($string, 0, 1) === $wd) {
                if ($uriParts[$key] === '') {
                    return false;
                }
                
                // Strip out the wildcard values.
                // They represent parameters.
                $parameterName = substr($string, 1);
                $parameterValue = $uriPart;
                
                $parameters[$parameterName] = $parameterValue;
            } elseif ($string !== $uriPart) {
                return false;
            }
        }
        
        return $parameters;
    }
    
    public function __toString()
    {
        return $this->pattern;
    }
}
