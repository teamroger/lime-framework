<?php

interface Lime_Route_DispatcherInterface
{
    public function dispatch(Lime_Route $route, Lime_Http_Server $server);
}
