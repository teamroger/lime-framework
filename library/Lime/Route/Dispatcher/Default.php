<?php

class Lime_Route_Dispatcher_Default implements Lime_Route_DispatcherInterface
{
    public function dispatch(Lime_Route $route, Lime_Http_Server $server)
    {
        $controllerNamingConvention = new App_Config_ControllerNamingConvention();
        
        $front = Lime_Controller_Front::getInstance();
        $front->dispatch($route, $server, $controllerNamingConvention);
        
        /*
        $request = $server->getRequest();
        $response = $server->getResponse();

        $body = (string)$request;

        $response
        ->setCode(200)
        ->setPhrase('OK')
        ->addHeader(new Lime_Http_Header('Content-Type', 'text/plain'))
        ->setBody((string) $request);
        */
    }
}
