<?php

/**
 * Generates random strings based on an alphabet of characters to use.
 */
class Lime_String_RandomGenerator
{
    private $alphabet;
    
    public function __construct()
    {
        $this->alphabet = '';
        
        // Add lower case letters.
        // Omit lower case "L".
        $this->alphabet .= 'abcdefghijkmnopqrstuvwxyz';
        
        // Add upper case letters.
        // Omit "I" and "O".
        $this->alphabet .= 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        
        // Add digits.
        // Omit "0".
        $this->alphabet .= '123456789';
    }
    
    /**
     * Gets the alphabet being used to generate the random string.
     *
     * @return string The alphabet.
     */
    public function getAlphabet()
    {
        return $this->alphabet;
    }
    
    /**
     * Sets the alphabet to use to generate the random string.
     *
     * @param  string $alphabet The alphabet.
     * @return Lime_String_RandomGenerator This random string generator.
     */
    public function setAlphabet($alphabet)
    {
        $alphabet = (string)$alphabet;
        
        // Ensure alphabet is not empty.
        if (strlen($alphabet) === 0) {
            throw new InvalidArgumentException('Alphabet cannot be empty');
        }
        
        $this->alphabet = $alphabet;
        
        return $this;
    }
    
    /**
     * Generates a random srtring.
     *
     * @param  int The length of the string to be created.
     * @return string The randomly generated string.
     */
    public function generate($length = 8)
    {
        $length = (int)$length;
        $alphabetLength = strlen($this->alphabet);
        $randomString = '';
        
        for ($i = 0; $i < $length; $i++) {
            // Find a random point in the alphabet.
            $randomIndex = rand(0, $alphabetLength - 1);
            
            // Select the character at that point in the alphabet.
            $randomCharacter = substr($this->alphabet, $randomIndex, 1);
            
            // Append it to the random string.
            $randomString .= $randomCharacter;
        }
        
        return $randomString;
    }
    
    /**
     * Gets a string representation of this object.
     *
     * @return A randomly generated string.
     */
    public function __toString()
    {
        return $this->generate();
    }
}
