<?php

/**
 * To be used with the Lime_String class to truncate strings.
 */
class Lime_String_Operation_Truncate implements Lime_String_OperationInterface
{
    protected $parameters = array();
    
    /**
     * @param  array $parameters An array of parameters:
     *                           1) The length to truncate the string down to.
     *                           2) An optional string to add to the end of the newly truncated string.
     *                           For example "..."
     * @return void
     */
    public function preApply(array $parameters)
    {
        $this->parameters = $parameters;
    }
    
    /**
     * @param  Lime_String $string The string you wish to truncate.
     * @return Lime_String The newly truncate string as a Lime_String.
     */
    public function apply(Lime_String $string)
    {
        $newString = substr($string, 0, $this->parameters[0]);
        
        if (isset($this->parameters[1])) {
            if (strlen($this->parameters[1]) > 0) {
                $newString .= $this->parameters[1];
            }
        }
        
        return Lime_String::create($newString);
    }
}
