<?php

/**
 * Sets out the requirements for a string operation.
 */
interface Lime_String_OperationInterface
{
    
    /**
     * Sets the parameters that will be used to perform
     * the implemented operation.
     *
     * @param $parameters array The paramaters that the operation will need
     * to do its job.
     *
     * @return void
     */
    public function preApply(array $parameters);
    
    /**
     * Applies the string operation to the given Lime_String object
     * using any paramaters that were given in the preApply method.
     *
     * @param  $string Lime_String The string to apply to operation to.
     * @return Lime_String A transformed version of the original string.
     */
    public function apply(Lime_String $string);
}
