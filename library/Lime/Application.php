<?php

class Lime_Application
{
    protected $server = null;
    protected $setup = null;
    protected $controller = null;
    
    public function __construct(
        Lime_Http_Server $server,
        Lime_Application_Setup $setup,
        Lime_Controller_Action $controller
    ) {
        $this->server = $server;
        $this->setup = $setup;
        $this->controller = $controller;
    }
    
    public function start()
    {
        $this->setup->run($this->controller);
        $this->controller->dispatch();
        $this->server->sendResponse();
    }
}
