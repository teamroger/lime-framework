<?php

interface Lime_ErrorHandler
{
    public function handleError(Exception $e);
}
