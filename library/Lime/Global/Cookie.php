<?php

class Lime_Global_Cookie
{
    protected static $instance = null;
    
    private function __construct()
    {
        // Singleton.
    }
    
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Lime_Global_Cookie();
        }
        
        return self::$instance;
    }
    
    public function has($key)
    {
        return array_key_exists($key, $COOKIE);
    }
    
    public function get($key)
    {
        return $COOKIE[$key];
    }
    
    public function set($name, $value, $expires = 0)
    {
        setcookie($name, $value, $expires);
    }
}
