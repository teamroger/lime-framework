<?php

class Lime_Global_Get
{
    protected static $instance = null;
    
    private function __construct()
    {
        // Singleton.
    }
    
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Lime_Global_Get();
        }
        
        return self::$instance;
    }
    
    public function has($key)
    {
        return array_key_exists($key, $_GET);
    }
    
    public function get($key)
    {
        return filter_input(INPUT_GET, $key, FILTER_DEFAULT);
    }
}
