<?php

class Lime_Global_Post
{
    protected static $instance = null;
    
    private function __construct()
    {
        // Singleton.
    }
    
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Lime_Global_Post();
        }
        
        return self::$instance;
    }
    
    public function has($key)
    {
        return array_key_exists($key, $_POST);
    }
    
    public function get($key)
    {
        return filter_input(INPUT_POST, $key, FILTER_DEFAULT);
    }
}
