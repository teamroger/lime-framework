<?php

class Lime_Upload_File
{
    private $filename;
    private $tempName;
    private $fileSize;
    private $type;
    
    public function __construct($filename, $tempName, $fileSize, $type)
    {
        $this->filename = (string)$filename;
        $this->tempName = (string)$tempName;
        $this->fileSize = (int)$fileSize;
        $this->type = (string)$type;
    }
    
    public function move($destination, $newName = null)
    {
        if (!$newName) {
            $newName = $this->filename;
        }
        
        if (!is_dir($destination)) {
            throw new Exception('Destination folder: "' . $destination . '" does not exist', 0);
        }
        
        if (!is_writable($destination)) {
            throw new Exception('Destination folder: "' . $destination . '" is not writable', 1);
        }
        
        move_uploaded_file($this->tempName, $destination . '/' . $newName);
    }
    
    public function getFilename()
    {
        return $this->filename;
    }
    
    public function getTempName()
    {
        return $this->tempName;
    }
    
    public function getFilesize()
    {
        return $this->fileSize;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function getExtension()
    {
        // Get the index of the right-most dot.
        $indexOfDot = strrpos($this->filename, '.');
        
        // If there is no extension.
        if ($indexOfDot === false) {
            return null;
        }
        
        // If the dot is the last character.
        if ($indexOfDot == strlen($this->filename) - 1) {
            return '';
        }
        
        // Return everything after the dot.
        return substr($this->filename, $indexOfDot + 1);
    }
}
