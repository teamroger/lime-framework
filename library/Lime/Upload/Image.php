<?php

class Lime_Upload_Image extends Lime_Upload_File
{
    private $width;
    private $height;
    
    public function __construct($filename, $tempName, $fileSize, $type)
    {
            parent::__construct($filename, $tempName, $fileSize, $type);

            $imageFormats = array('jpg', 'jpeg', 'pjpg', 'png', 'bmp', 'gif');

        if (!in_array(strtolower($this->getExtension()), $imageFormats)) {
                throw new InvalidArgumentException('Extension "' . $this->getExtension() . '" is not an accepted image extension');
        }
    }
    
    public function getWidth()
    {
        if (is_null($this->width)) {
            $this->detectDimensions();
        }
        
        return $this->width;
    }
    
    public function getHeight()
    {
        if (is_null($this->height)) {
            $this->detectDimensions();
        }
        
        return $this->height;
    }
    
    private function detectDimensions()
    {
        $imageProperties = getimagesize($this->getFilename());
        
        $this->width = $imageProperties[0];
        $this->height = $imageProperties[1];
    }
}
