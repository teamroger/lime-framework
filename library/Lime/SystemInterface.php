<?php

interface Lime_SystemInterface
{
    public function init(Lime_Registry $registry);
    public function getRegistryKeysCreated();
}
