<?php

class Lime_Route
{
    protected $resourceType = '';
    protected $action = '';
    protected $parameters = array();
    
    public function __construct(
        $resourceType = '',
        $action = ''
    ) {
        $this
            ->setResourceType($resourceType)
            ->setAction($action);
    }
    
    public function getResourceType()
    {
        return $this->resourceType;
    }
    
    public function setResourceType($resourceType)
    {
        $this->resourceType = (string) $resourceType;
        
        return $this;
    }
    
    public function getAction()
    {
        return $this->action;
    }
    
    public function setAction($action)
    {
        $this->action = (string) $action;
        
        return $this;
    }
    
    public function getParameters()
    {
        return $this->parameters;
    }
    
    public function addParameter($name, $value)
    {
        $this->parameters[(string) $name] = (string) $value;
        
        return $this;
    }
}
