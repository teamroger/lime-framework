<?php

class Lime_Registry
{
    private static $instance = null;
    private $data = array();
    
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Lime_Registry();
        }
        
        return self::$instance;
    }
    
    public function get($key)
    {
        $key = (string)$key;
        
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        } else {
            include_once 'Lime/Registry/Exception.php';
            
            throw new Lime_Registry_Exception('Key "' . $key . '" does not exist');
        }
    }
    
    public function has($key)
    {
        return array_key_exists($key, $this->data);
    }
    
    public function set($key, $value)
    {
        $key = (string)$key;
        $this->data[$key] = $value;
        
        return $this;
    }
}
