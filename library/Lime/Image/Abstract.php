<?php

/**
 * Models an image file.
 * Provides functionality to get attributes of the image such as width and height and
 * also to manipulate the image through image operations.
 *
 * This class is abstract and must be extended such that the sub-class implements
 * the image-specific functionality.
 *
 * @author Roger Barnfather
 */
abstract class Lime_Image_Abstract
{
    protected $filename;
    protected $image;

    /**
     * Opens an image file.
     *
     * @param $filename String The file to open.
     */
    protected function open($filename)
    {
        if (file_exists($filename)) {
            if (is_readable($filename)) {
                // Delagate the specific opening method to the sub-class.
                $image = $this->createFromFile($filename);

                if ($image === false) {
                    throw new Exception('Could not open image file "' . $filename . '"');
                }

                $this->image = $image;
                $this->filename = $filename;
            } else {
                throw new InvalidArgumentException('File "' . $filename . '" is not readable');
            }
        } else {
                throw new InvalidArgumentException('File "' . $filename . '" does not exist');
        }
    }

    /**
     * Provides the format-specific way to open a file.
     * E.g.- An extending class for JPEGs would open images using "imaagecreatefromjpeg()".
     *
     * @param $filename String the image file to open.
     */
    abstract protected function createFromFile($filename);

    /**
     * Outputs this file's contents to the standard output.
     * The extending class is expected to send the relevant content-type header.
     */
    abstract public function render();

    /**
     * Saves this image to disk.
     * The extending class implements what format the image is saved as.
     *
     * @param $filename String The new filename (or full path). If none is given then the
     * original file will be overwritten.
     */
    abstract public function save($filename = '');

    /**
     * Constructs an image.
     *
     * @param $filename String The image file to open.
     */
    public function __construct($filename)
    {
        $this->open($filename);
    }

    /**
     * Destroys the in-memory image data.
     */
    public function __destruct()
    {
        imagedestroy($this->image);
    }

    /**
     * Applies an image operation to this image.
     *
     * @param  $operation Lime_Image_Operation_Interface A valid image operation.
     * @return Lime_Image_Abstract This image.
     */
    public function applyOperation(Lime_Image_Operation_Interface $operation)
    {
        $this->image = $operation->apply($this->image);
        return $this;
    }

    /**
     * Gets the filename of this image.
     *
     * @return String The filename of this image.
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Gets the size in bytes of this image.
     *
     * @return Integer The size in bytes of this image.
     */
    public function getSize()
    {
        return getimagesize($this->filename);
    }

    /**
     * Gets the width in pixels of this image.
     *
     * @return Integer The width in pixels of this image.
     */
    public function getWidth()
    {
        return imagesx($this->image);
    }

    /**
     * Gets the height in pixels of this image.
     *
     * @return Integer The height in pixels of this image.
     */
    public function getHeight()
    {
        return imagesy($this->image);
    }
}
