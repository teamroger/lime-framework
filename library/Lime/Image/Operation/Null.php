<?php

class Lime_Image_Operation_Null implements Lime_Image_Operation_Interface
{
    private $imageResource;
    
    public function __construct($imageResource)
    {
        $this->imageResource = $imageResource;
    }
    
    public function apply()
    {
    }
}
