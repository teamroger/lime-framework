<?php

class Lime_Image_Operation_Crop implements Lime_Image_Operation_Interface
{
    private $x1 = 0;
    private $y1 = 0;
    private $x2 = 1;
    private $y2 = 1;
    private $newWidth = 1;
    private $newHeight = 1;
    
    public function __construct($x1, $y1, $x2, $y2, $newWidth, $newHeight)
    {
        $this->x1 = $x1;
        $this->y1 = $y1;
        $this->x2 = $x2;
        $this->y2 = $y2;
        $this->newWidth = $newWidth;
        $this->newHeight = $newHeight;
    }
    
    public function apply($imageResource)
    {
        $width = imagesx($imageResource);
        $height = imagesy($imageResource);
        
        $newImage = imagecreatetruecolor($this->newWidth, $this->newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if (imagecopyresampled($newImage, $imageResource, 0, 0, $this->x1, $this->y1, $this->newWidth, $this->newHeight, $this->x2 - $this->x1, $this->y2 - $this->y1)) {
            return $newImage;
        } else {
            return false;
        }
    }
}
