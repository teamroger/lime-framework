<?php

class Lime_Image_Operation_Resize implements Lime_Image_Operation_Interface
{
    private $newWidth = 1;
    private $newHeight = 1;
    
    public function __construct($newWidth, $newHeight)
    {
        $this->newWidth = $newWidth;
        $this->newHeight = $newHeight;
    }
    
    public function apply($imageResource)
    {
        $newImage = imagecreatetruecolor($this->newWidth, $this->newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if (imagecopyresampled($newImage, $imageResource, 0, 0, 0, 0, $this->newWidth, $this->newHeight, imagesx($imageResource), imagesy($imageResource))) {
            return $newImage;
        } else {
            return false;
        }
    }
}
