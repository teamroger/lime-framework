<?php

class Lime_Image_Operation_Scale implements Lime_Image_Operation_Interface
{
    private $factor = 1;
    
    public function __construct($factor)
    {
        $this->factor = $factor;
    }
    
    public function apply($imageResource)
    {
        $width = imagesx($imageResource);
        $height = imagesy($imageResource);
        $newWidth = (int)($this->factor * $width);
        $newHeight = (int)($this->factor * $height);
        
        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if (imagecopyresampled($newImage, $imageResource, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height)) {
            return $newImage;
        } else {
            return false;
        }
    }
}
