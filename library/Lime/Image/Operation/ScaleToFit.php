<?php

class Lime_Image_Operation_ScaleToFit implements Lime_Image_Operation_Interface
{
    private $newWidth = 1;
    private $newHeight = 1;
    private $margin = 0;
    
    public function __construct($newWidth, $newHeight, $margin = 0)
    {
        $this->newWidth = $newWidth;
        $this->newHeight = $newHeight;
        $this->margin = $margin;
    }
    
    public function apply($imageResource)
    {
        $width = imagesx($imageResource);
        $height = imagesy($imageResource);
        
        $newImage = imagecreatetruecolor($this->newWidth, $this->newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if ($width > $height) {
            $newWidth = floor($this->newWidth - $this->margin * 2);
            $newHeight = floor($this->newWidth / $width * $height);
            $newX = $this->margin;
            $newY = floor(($this->newHeight - $newHeight) / 2);
        } else {
            $newHeight = floor($this->newHeight - $this->margin * 2);
            $newWidth = floor($height / $height * $width);
            $newY = $this->margin;
            $newX = floor(($this->newWidth - $newWidth) / 2);
        }
        
        if (imagecopyresampled($newImage, $imageResource, $newX, $newY, 0, 0, $newWidth, $newHeight, $width, $height)) {
            return $newImage;
        } else {
            return false;
        }
    }
}
