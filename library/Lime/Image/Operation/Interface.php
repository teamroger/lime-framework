<?php

interface Lime_Image_Operation_Interface
{
    public function apply($imageResource);
}
