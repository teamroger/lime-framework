<?php

class Lime_Image_Png extends Lime_Image_Abstract
{
    protected $quality = 0.75;
    
    protected function createFromFile($filename)
    {
        return imageCreateFromPng($filename);
    }
    
    public function getQuality()
    {
        return $this->quality;
    }
    
    public function setQuality($quality)
    {
        if (!is_numeric($quality) or $quality < 0 or $quality > 1) {
            throw new Exception('Quality must be a number between 0 and 1');
        }
        
        $this->quality = $quality;
    }
    
    public function save($filename = '')
    {
        if ($filename) {
            return !(imagePng($this->image, $filename, $this->pngQuality()) == false);
        } else {
            return !(imagePng($this->image, $this->filename, $this->pngQuality()) == false);
        }
    }
    
    public function render()
    {
        header('Content-Type: image/png');
        imagePng($this->image, $this->pngQuality(), null);
    }
    
    protected function pngQuality()
    {
        $quality = $this->quality * 10;
        $quality = ceil($quality);
        $quality--;
        
        return $quality;
    }
}
