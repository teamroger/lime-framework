<?php

interface Lime_Image_Cropper_Step
{
    public function performAction();
    public function hasErrors();
    public function getErrors();
}
