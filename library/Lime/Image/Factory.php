<?php

/**
 * Creates concrete instances of Lime_Image_Abstract.
 */
class Lime_Image_Factory
{
    protected static $instance;

    private function __construct()
    {
    }

    /**
     * Gets the factory's instance.
     *
     * @return Lime_Image_Factory The image factory.
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Lime_Image_Factory();
        }

        return self::$instance;
    }

    /**
     * Creates the apropriate image object based on the given filename.
     *
     * @param  $filename String The image filename.
     * @return Lime_Image_Abstract An image object if the file is a recognised image otherwise NULL.
     */
    public function create($filename)
    {
        $extension = strtolower(substr($filename, strrpos($filename, '.') + 1));

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                return new Lime_Image_Jpeg($filename);
            
            case 'png':
                return new Lime_Image_Png($filename);
            
            default:
                return null;
        }
    }
}
