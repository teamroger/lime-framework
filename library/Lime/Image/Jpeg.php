<?php

class Lime_Image_Jpeg extends Lime_Image_Abstract
{
    protected $quality = 0.75;
    
    protected function createFromFile($filename)
    {
        return imageCreateFromJpeg($filename);
    }
    
    public function getQuality()
    {
        return $this->quality;
    }
    
    public function setQuality($quality)
    {
        if (!is_numeric($quality) or $quality < 0 or $quality > 1) {
            throw new Exception('Quality must be a number between 0 and 1');
        }
        
        $this->quality = $quality;
    }
    
    public function save($filename = '')
    {
        if ($filename) {
            return !(imageJpeg($this->image, $filename, $this->jpegQuality()) === false);
        } else {
            return !(imageJpeg($this->image, $this->filename, $this->jpegQuality()) === false);
        }
    }
    
    public function render()
    {
        header('Content-Type: image/jpeg');
        return imageJpeg($this->image, null, $this->jpegQuality());
    }
    
    protected function jpegQuality()
    {
        return (int)($this->getQuality() * 100);
    }
}
