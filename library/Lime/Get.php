<?php

class Lime_Get
{
    public static function has($key)
    {
        return Lime_Global_Get::getInstance()->has($key);
    }
    
    public static function get($key)
    {
        return Lime_Global_Get::getInstance()->get($key);
    }
}
