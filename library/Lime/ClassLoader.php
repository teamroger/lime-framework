<?php

require_once 'Lime/Exception.php';

class Lime_ClassLoader
{
    protected $dir = '';
    
    public function __construct($dir = '')
    {
        $this->setDir($dir);
    }
    
    public function getDir()
    {
        return $this->dir;
    }
    
    public function setDir($directory)
    {
        $directory = realpath($directory);
        
        if (! file_exists($directory)) {
            throw new Lime_Exception('Directory does not exist: "' . $directory . '"');
        }
        
        if (! is_dir($directory)) {
            throw new Lime_Exception('Path is not a directory: "' . $directory . '"');
        }
        
        $this->dir = $directory;
        
        return $this;
    }
    
    public function load(
        $filename,
        $classname,
        array $args = array(),
        $abstractClass = 'object'
    ) {
        // Throw an exception if the directory has not been set.
        if ($this->getDir() === '') {
            throw new Lime_Exception('Directory not set.');
        }
        
        return $this->createInstance(
            $filename,
            $classname,
            $args,
            $abstractClass
        );
    }
    
    protected function createInstance(
        $filename,
        $classname,
        array $args = array(),
        $abstractClass = 'object'
    ) {
        $this->loadFile($filename);
        
        if (class_exists($classname)) {
            $class = new ReflectionClass($classname);
            $instance = $class->newInstance($args);
        } else {
            throw new Lime_Exception('Class does not exist: "' . $classname . '"');
        }
        
        if (! ($instance instanceof $abstractClass)) {
            $message = 'Class "' . get_class($instance) . '" ' .
                    'must extend "' . $abstractClass . '".';
            throw new Lime_Exception($message);
        }
        
        return $instance;
    }
    
    protected function loadFile($filename)
    {
        $path = $this->getDir() . '/' . $filename;
        
        if (! file_exists($path)) {
            throw new Lime_Exception('File does not exist: "' . $path . '"');
        }
        
        if (! is_readable($path)) {
            throw new Lime_Exception('Flie is not readable: "' . $path . '"');
        }
        
        include_once $path;
    }
}
