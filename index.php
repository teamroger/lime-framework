<?php

final class Main {
	public static final function init() {
		require_once 'app/config/AutoLoader.php';
		
		$autoloader = new App_Config_Autoloader();
		$autoloader->init();
        
        $container = new Lime_Application_Container();
        $container->init();
        
        $config = new App_Config_Container($container);
        $config->init();
        
        $application = $container->resolve('application');
        
		$application->start();
		
		exit;
	}
}

Main::init();