<?php

class App_Controller_Index extends Lime_Controller_Action
{
    public function __construct(Lime_Http_Server $server, $action, array $parameters)
    {
        parent::__construct($server, $action, $parameters);
    }
    
    public function index()
    {
        $layout = $this->getPlugin('layout');
        $request = $this->getServer()->getRequest();
        
        $layout
            ->set('title', 'Index | New Website')
            ->getTemplate()
            ->set('items', $this->getItems());
        
        $statuses = array(
            0 => 'New item has been created.',
            1 => 'Changes have been saved.',
            2 => 'Item was deleted.'
        );
        
        if ($request->queryVars('status') !== null
            && isset($statuses[$request->queryVars('status')])
        ) {
            $layout->getTemplate()
                ->set('message', $statuses[$request->queryVars('status')]);
        }
    }
    
    public function edit($id)
    {
        $form = new Lime_Template(
            'app\view\template\index\partial\form.phtml'
        );
        
        $form->set('action', '/index/' . $id);
        
        $this->getPlugin('layout')
            ->set('title', 'Edit | New Website')
            ->getTemplate()
            ->set('id', $id)
            ->set('form', $form);
    }
    
    public function update($id)
    {
        $this->getPlugin('redirect')
            ->redirectTo('/index?status=1');
    }
    
    public function create()
    {
        $form = new Lime_Template(
            'app\view\template\index\partial\form.phtml'
        );
        
        $form->set('action', '/index');
        
        $this->getPlugin('layout')
            ->set('title', 'Create | New Website')
            ->getTemplate()
            ->set('form', $form);
    }
    
    public function save()
    {
        $this->getPlugin('redirect')
            ->redirectTo('/index?status=0');
    }
    
    public function delete($id)
    {
        $this->getPlugin('redirect')
            ->redirectTo('/index?status=2');
    }
    
    protected function getItems()
    {
        return array(
            array(
                'id' => '1',
                'name' => 'Item 1'
            ),
            array(
                'id' => '2',
                'name' => 'Item 2'
            ),
            array(
                'id' => '3',
                'name' => 'Item 3'
            )
        );
    }
}
