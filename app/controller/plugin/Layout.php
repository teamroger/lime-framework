<?php

class App_Controller_Plugin_Layout extends Lime_Template implements Lime_Controller_PluginInterface
{
    protected $enabled = true;
    protected $resourceType = null;
    protected $action = null;
    protected $childTemplate = null;
    
    public function __construct($resourceType, $action)
    {
        $this->resourceType = $resourceType;
        $this->action = $action;
    }
    
    public function setEnabled($enabled)
    {
        $this->enabled = (boolean) $enabled;
        
        return $this;
    }
    
    public function getEnabled()
    {
        return $this->enabled;
    }
    
    public function getTemplate()
    {
        return $this->childTemplate;
    }
    
    public function preDispatch(Lime_Controller_Action $controller)
    {
        $ds = DIRECTORY_SEPARATOR;
        
        $this->setFilename(
            'app' .
            $ds .
            'view' .
            $ds .
            'layout' .
            $ds .
            $this->resourceType .
            '.phtml'
        );
        
        $this->childTemplate = new Lime_Template(
            'app' .
            $ds .
            'view' .
            $ds .
            'template' .
            $ds .
            $this->resourceType .
            $ds .
            $this->action .
            '.phtml'
        );
        
        $this->set('content', $this->getTemplate());
    }
    
    public function postDispatch(Lime_Controller_Action $controller)
    {

        if ($this->getEnabled()) {
            $response = $controller->getServer()->getResponse();
            $response->setBody($this);
        }
    }
}
