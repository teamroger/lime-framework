<?php

class App_Controller_Plugin_Redirect implements Lime_Controller_PluginInterface
{
    protected $controller;
    
    public function redirectTo($location, $permanent = false)
    {
        $this->controller->getServer()->getResponse()
            ->setHttpVersion(1.1)
            ->setCode(302)
            ->setPhrase($permanent ? 'Moved permanently' : 'Moved Temporarily')
            ->addHeader(
                new Lime_Http_Header(
                    'Location',
                    $location
                )
            )
            ->setBody('');
    }
    
    public function preDispatch(Lime_Controller_Action $controller)
    {
        $this->controller = $controller;
    }
    
    public function postDispatch(Lime_Controller_Action $controller)
    {
    }
}
