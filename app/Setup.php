<?php

class App_Setup extends Lime_Application_Setup
{
    protected $route = null;
    
    public function __construct(Lime_Route $route)
    {
        $this->route = $route;
    }
    
    public function setupTimezone()
    {
        date_default_timezone_set('Europe/London');
    }
    
    public function setupLayout()
    {
        $controller = $this->getActionController();
        
        if ($this->route !== null) {
            $layoutPlugin = new App_Controller_Plugin_Layout(
                $this->route->getResourceType(),
                $this->route->getAction()
            );
            
            $controller->setPlugin('layout', $layoutPlugin);
        }
    }
    
    public function setupRedirect()
    {
        $controller = $this->getActionController();
        
        $redirectPlugin = new App_Controller_Plugin_Redirect();
        
        $controller->setPlugin('redirect', $redirectPlugin);
    }
    
    public function setupExceptionHandler()
    {
        set_exception_handler(array(new App_Config_ExceptionHandler(), 'handle'));
    }
}
