<?php

class App_Config_ExceptionHandler
{
    public static function handle(Exception $e)
    {
        $response = new Lime_Http_Response();
        $response->setHttpVersion(1.1);
        $responseSender = new Lime_Http_Response_Sender();
        $exceptionClass = get_class($e);
        
        switch ($exceptionClass) {
            case 'Lime_Http_Exception_NotFound':
                $response->setCode(404)->setPhrase('Not found');
                $template = new Lime_Template('app\view\error\not-found.phtml');
                break;
            
            default:
                $response->setCode(500)->setPhrase('Internal server error');
                $template = new Lime_Template('app\view\error\default.phtml');
                break;
        }
        
        $response->setBody($template);
        $responseSender->send($response);
    }
}
