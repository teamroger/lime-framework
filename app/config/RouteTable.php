<?php

class App_Config_RouteTable extends Lime_Route_Table
{
    public function __construct()
    {
        $this->set(
            'GET',
            array('/' => new Lime_Route('Index', 'index'))
        );
        
        $this->set(
            'GET',
            array(
                '/index' => new Lime_Route('Index', 'index'),
                '/index/new' => new Lime_Route('Index', 'create'),
                '/index/:id' => new Lime_Route('Index', 'show'),
                '/index/:id/edit' => new Lime_Route('Index', 'edit'),
                '/index/:id/delete' => new Lime_Route('Index', 'delete')
            )
        );
        
        $this->set(
            'POST',
            array(
                '/' => new Lime_Route('Index', 'save'),
                '/index' => new Lime_Route('Index', 'save'),
                '/index/:id' => new Lime_Route('Index', 'update')
            )
        );
    }
}
