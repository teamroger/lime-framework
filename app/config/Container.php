<?php

class App_Config_Container
{
    protected $container = null;

    public function __construct(Lime_IoC_Container $container)
    {
        $this->container = $container;
    }

    public function init()
    {
        $baseValues = array(
            'sessionKey' => 'LIME_SESSION_ID',
            'sessionLifetime' => 60 * 10,
            'sessionDir' => '.' . DIRECTORY_SEPARATOR . 'session'
        );

        $this->container
            ->registerBaseValues($baseValues)
            ->register('appSetup', 'App_Setup', array('route'), true)
            ->register('namingConvention', 'App_Config_ControllerNamingConvention', array(), true)
            ->register('sessionStorage', 'Lime_Http_Session_Storage_File', array('sessionDir'), true)
            ->register('routeTable', 'App_Config_RouteTable', array(), true);
    }
}
