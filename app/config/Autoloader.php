<?php

class App_Config_Autoloader
{
    public function init()
    {
        spl_autoload_register(array($this, 'load'));
    }
    
    public function load($class)
    {
        $d = DIRECTORY_SEPARATOR;
        
        $parts = explode('_', $class);
        
        switch ($parts[0]) {
            case 'Lime':
                array_shift($parts);
                $path = 'library' . $d . 'Lime' . $d . implode($d, $parts) . '.php';
                
                require_once $path;
                break;
            
            case 'App':
                foreach ($parts as $key => $value) {
                    $parts[$key] = ucfirst($value);
                }
                
                $path = implode($d, $parts) . '.php';
                
                require_once $path;
                break;
        }
    }
}
