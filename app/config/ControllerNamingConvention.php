<?php

class App_Config_ControllerNamingConvention implements
    Lime_Controller_NamingConventionInterface
{
    public function formatFilename($controllerName)
    {
        return
            'app' .
            DIRECTORY_SEPARATOR .
            'controller' .
            DIRECTORY_SEPARATOR .
            $controllerName . '.php';
    }
    
    public function formatClassname($controllerName)
    {
        return 'App_Controller_' . $controllerName;
    }
}
